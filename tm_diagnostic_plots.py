###outputs basic diagnostic plots for CIRADA Transient Marshal QA
###are we doing the table joins in here, or assume this has been done in advance?


import numpy as np, matplotlib.pyplot as plt, os, sys, argparse
from astropy.table import Table, Column, join, unique, vstack, hstack
from astropy.coordinates import SkyCoord
from astropy import units as u
from scipy.stats import distributions as dist
from matplotlib.colors import LogNorm
from distutils import util


plt.interactive(True) ###remove when code ready, will output plots to file


#######################################################################
#######################################################################
###parameters

###remove these once made command line arguments

tmcand_file = 'test_data/test_tmcandidates.fits'
tmsetcomp_file = 'test_data/test_tmsetcomponents.fits'
tmcomp_file = 'test_data/test_tmcomponent.fits'
tmcompset_file = 'test_data/test_tmcomponentset.fits'

tm_prejoined_file = 'test_data/test_prejoined_tmdata.fits'


#######################################################################
#######################################################################
###functions


def load_separate_and_join(candfile, scfile, cfile, csfile):
    'if loading separate tables that need joining for the diagnostics'
    
    #load individual tables
    cand = Table.read(candfile)
    setcomps = Table.read(scfile)
    comps = Table.read(cfile)
    csets = Table.read(csfile)
    
    #astropy requires the same key to join on and 'id' has different meaning in these tables
    #set up exlplicit xid_s/xid_c for component_set and component_id in order to join
    comps['xid_c'] = comps['id']
    comps['xid_s'] = comps['component_set_id']
    setcomps['xid_s'] = setcomps['component_set_id']
    csets['xid_s'] = csets['id']
    cand['xid_s'] = cand['component_set_id']

    ###join data - name tables appropriately to remove conflicting colnames or rename id columnnames
    comps.rename_column(name='id', new_name='temporalcomponent_id')
    setcomps.rename_column(name='id', new_name='setcomponents_id')
    csets.rename_column(name='id', new_name='temporalcomponentset_id')
    cand.rename_column(name='id', new_name='candidate_id')

    ##join components with compsets
    data = join(comps, setcomps, keys='xid_s', join_type='left')
    ##add in set and candidate data
    data = join(data, csets, keys='xid_s', join_type='left')
    data = join(data, cand, keys='xid_s', join_type='left')
    
    ###remove added columns (duplicate info only needed to join tables in astropy)
    data.remove_columns(names=['xid_c', 'xid_s'])

    return data


def load_prejoined(filename):
    'use if loading a joined data table that is the result of a query'
    
    data = Table.read(filename)
    
    return data


def binom_frac(k, n, conf=0.683, nround=5):
    'return fraction and binomial errors based on confidence level assuming beta dist, see also https://ui.adsabs.harvard.edu/abs/2011PASA...28..128C/abstract'
        
    frac = k/n
    p_low = dist.beta.ppf((1-conf)/2, k+1, n-k+1)
    p_up = dist.beta.ppf(1-(1-conf)/2, k+1, n-k+1)
        
    el = p_low - frac
    eu = p_up - frac
        
    fracpluserrs = (np.round(frac, nround), np.round(el, nround), np.round(eu, nround))
        
    return fracpluserrs



###plotting functions:

def compare_flux_dists(s_main, s_comp,
                       xbins=None,
                       c0='C0',
                       c1='C3',
                       ls0='-',
                       ls1='--',
                       lw=1.5,
                       lab0='all',
                       lab1='comparison',
                       title=' ',
                       xlab=r'$S$ [mJy]',
                       fontsize=13,
                       savefig=False,
                       outname='flux_comparison.png'):
    
    'produce plot comparing two flux distributions, log-scaled so only plots positive values'
    
    ##make sure data is np array and check for nans that will break plot
    s_main = np.array(s_main)
    s_comp = np.array(s_comp)
    old_main_n = len(s_main)
    old_comp_n = len(s_comp)
    s_main = s_main[~np.isnan(s_main)]
    s_comp = s_comp[~np.isnan(s_comp)]
    ##warn user if any nans removed
    if len(s_main) != old_main_n:
        ndif_main = old_main_n - len(s_main)
        print('WARNING: ' + str(ndif_main) + ' nan values in s_main distibution, these have been filtered out when plotting')
    if len(s_comp) != old_comp_n:
        ndif_comp = old_comp_n - len(s_comp)
        print('WARNING: ' + str(ndif_comp) + ' nan values in s_comp distibution, these have been filtered out when plotting')
    
    #if no bins provided determine suitable min/max and n bins based on data
    if xbins is None:
        #lower limit of bins needs to filter out negative flux values that may exist in forced photo
        x0 = np.round(np.log10(np.min([np.min(s_main[(s_main>0)]),
                                       np.min(s_comp[(s_comp>0)])]))-0.5, 1)
        x1 = np.round(np.log10(np.max([np.max(s_main), np.max(s_comp)]))+0.5, 1)
        nbins = np.sqrt(np.min([len(s_main), len(s_comp)]))
        xbins = np.logspace(x0, x1, nbins)

    ##plot figure
    fig = plt.figure(figsize=(6,6))
    plt.hist(s_main, xbins, histtype='step', color=c0, ls=ls0, label=lab0, lw=lw)
    plt.hist(s_comp, xbins, histtype='step', color=c1, ls=ls1, label=lab1, lw=lw)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(xlab, fontsize=fontsize)
    plt.ylabel(r'$N$', fontsize=fontsize)
    plt.title(title, fontsize=fontsize+2)
    plt.grid(ls=':')
    plt.legend()

    if savefig==True:
        plt.savefig(outname)
        plt.close()
    
    return


def plot_fluxratio_vs_sn(fluxratio, signoise,
                         databins=None,
                         xlab='signal to noise',
                         ylab='flux ratio',
                         title='Max flux ratio versus S/N',
                         cmap='viridis',
                         fontsize=13,
                         savefig=False,
                         outname='tm_fluxratio_vs_signoise.png'):
    'plot ratio of max/min flux as a function of SN'
    
    ###make sure arrays in case units in input
    fluxratio = np.array(fluxratio)
    signoise = np.array(signoise)
    
    ##if no bins provided determine based on data
    ##possibility of inf if any min fluxes are 0, needs accounting for
    if databins is None:
        srat_good = fluxratio[(np.isfinite(fluxratio)) & (fluxratio>0)]
        sn_good = signoise[(np.isfinite(signoise)) & (signoise>0)]
        
        f0 = np.log10(np.nanmin(srat_good))
        f1 = np.log10(np.nanmax(srat_good))
        nf = int(np.sqrt(len(fluxratio))/4)
        s0 = np.log10(np.nanmin(sn_good))
        s1 = np.log10(np.nanmax(sn_good))
        ns = int(np.sqrt(len(signoise))/4)
        
        databins = [np.logspace(s0, s1, ns), np.logspace(f0, f1, nf)]
    
    plt.figure(figsize=(6.5, 6))
    plt.hist2d(signoise, fluxratio, databins, norm=LogNorm(), cmap=cmap)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(xlab, fontsize=fontsize)
    plt.ylabel(ylab, fontsize=fontsize)
    plt.title(title, fontsize=fontsize+2)
    plt.colorbar(label=r'$N$')
    plt.tight_layout()
    
    if savefig==True:
        plt.savefig(outname)
        plt.close()
    
    return


def plot_fraction_of_disap(alldat, disdat,
                           xbins=None,
                           lower_data_limit=None,
                           upper_data_limit=None,
                           linecolor='C0',
                           lw=1.5,
                           etransp=0.3,
                           xlab='',
                           ylab=r'$N_{\rm{disap}}/N_{\rm{sets}}$',
                           title='',
                           fontsize=13,
                           savefig=False,
                           outname='disap_fraction.png'):
    
    'plot fraction and binom error of disappeared sources as function of flux or sig/noise'
    
    ##make sure data is np array and check for nans that will break plot
    alldat = np.array(alldat)
    disdat = np.array(disdat)
    old_all_n = len(alldat)
    old_dis_n = len(disdat)
    alldat = alldat[~np.isnan(alldat)]
    disdat = disdat[~np.isnan(disdat)]
    ##warn user if any nans removed
    if len(alldat) != old_all_n:
        ndif_all = old_all_n - len(alldat)
        print('WARNING: ' + str(ndif_all) + ' nan values in s_main distibution, these have been filtered out when plotting')
    if len(disdat) != old_dis_n:
        ndif_dis = old_dis_n - len(disdat)
        print('WARNING: ' + str(ndif_dis) + ' nan values in s_comp distibution, these have been filtered out when plotting')

    ##clip data? probably want to exclude outlying values for this plot
    ##if defined as argument will only plot between lower/upper data limits given
    if lower_data_limit is not None:
        alldat = alldat[(alldat>=lower_data_limit)]
        disdat = disdat[(disdat>=lower_data_limit)]
        print('WARNING: only data >= ' + str(lower_data_limit) + ' has been selected for plotting')
    if upper_data_limit is not None:
        alldat = alldat[(alldat<=upper_data_limit)]
        disdat = disdat[(disdat<=upper_data_limit)]
        print('WARNING: only data <= ' + str(upper_data_limit) + ' has been selected for plotting')


    #if no bins provided determine suitable min/max and n bins based on data
    if xbins is None:
    #lower limit of bins needs to filter out negative flux values that may exist in forced photo
        x0 = np.round(np.log10(np.min([np.min(alldat[(alldat>0)]),
                                       np.min(disdat[(disdat>0)])]))-0.5, 1)
        x1 = np.round(np.log10(np.max([np.max(alldat), np.max(disdat)]))+0.5, 1)
        nbins = np.sqrt(np.min([len(alldat), len(disdat)]))
        xbins = np.logspace(x0, x1, nbins)


    ##determine fraction of disappeareds as function of x
    n = np.histogram(alldat, xbins)[0]
    k = np.histogram(disdat, xbins)[0]
    frac, elo, eup = binom_frac(k=k, n=n)

    #use mid points of bins to plot x
    xvals = (xbins[1:]+xbins[:-1])/2

    ##plot fig
    fig = plt.figure(figsize=(6,6))
    plt.fill_between(xvals, frac+elo, frac+eup, color=linecolor, alpha=etransp)
    plt.plot(xvals, frac, c=linecolor, lw=lw)
    plt.xscale('log')
    plt.xlabel(xlab, fontsize=fontsize)
    plt.ylabel(ylab, fontsize=fontsize)
    plt.title(title, fontsize=fontsize+2)

    plt.grid(ls=':')

    if savefig==True:
        plt.savefig(outname)
        plt.close()

    return


def plot_compsep_dist(ra_comp, dec_comp, ra_set, dec_set,
                      posunit='deg',
                      sepunit='arcsec',
                      sepbins=None,
                      minbin=None,
                      histcolor='steelblue',
                      fontsize=13,
                      title='Positional offset of components from \n mean component-set position',
                      savefig=False,
                      outname='comp-set_separations.png'):
    'plot the distribution of component separations within a set to ID bad set collation'
    
    ##set up skycoordinates
    comppos = SkyCoord(ra=ra_comp, dec=dec_comp, unit=posunit)
    setpos = SkyCoord(ra=ra_set, dec=dec_set, unit=posunit)
    sep = comppos.separation(setpos).to(sepunit).value
    
    ###if wanting to stack all very small separations in a 'minimum' artificial bin use minbin to define this
    if minbin is not None:
        sep[sep<minbin] = minbin
    
    ###if no sepbins provided define these here based on input data
    if sepbins is None:
        s0 = np.log10(np.min(sep[sep>0]))
        s1 = np.log10(np.max(sep))
        ##with an artificial 'minbin' need binwidth to wide enough to be seen!
        if minbin is None:
            nbins = np.sqrt(len(sep))
        else:
            nbins = np.min([np.sqrt(len(sep)), 200])
        sepbins = np.logspace(s0, s1, nbins)


    ##incorporate sepunit into string for xlabel
    xlab = 'Angular separation [' + sepunit +']'

    ##plot figure
    fig = plt.figure(figsize=(6,6))
    plt.hist(sep, sepbins, color=histcolor)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(xlab, fontsize=fontsize)
    plt.ylabel(r'$N$', fontsize=fontsize)
    plt.title(title, fontsize=fontsize+2)
    plt.grid(ls=':')

    if savefig==True:
        plt.savefig(outname)
        plt.close()
    
    return


def plot_sep_by_coord(ra_comp, dec_comp, ra_set, dec_set,
                      bydecl=True,
                      xbinsize_deg=1,
                      posunit='deg',
                      sepunit='arcsec',
                      sepbins=None,
                      minbin=None,
                      cmap='viridis',
                      fontsize=13,
                      title='Positional offset of components from \n mean component-set position',
                      savefig=False,
                      outname='comp-set_separations_by_coord.png'):
    'plot separation as a function of RA or DEC -- DEC probably most useful'
    
    ##set up skycoordinates
    comppos = SkyCoord(ra=ra_comp, dec=dec_comp, unit=posunit)
    setpos = SkyCoord(ra=ra_set, dec=dec_set, unit=posunit)
    sep = comppos.separation(setpos).to(sepunit).value
    
    ###if wanting to stack all very small separations in a 'minimum' artificial bin use minbin to define this
    if minbin is not None:
        sep[sep<minbin] = minbin
    
    ###if no sepbins provided define these here based on input data
    if sepbins is None:
        s0 = np.log10(np.min(sep[sep>0]))
        s1 = np.log10(np.max(sep))
        ##with an artificial 'minbin' need binwidth to wide enough to be seen!
        if minbin is None:
            nbins = int(np.sqrt(len(sep))/4)
        else:
            nbins = np.min([int(np.sqrt(len(sep))/4), 100])
        sepbins = np.logspace(s0, s1, nbins)
            
    ###set up axes dependent on whether using RA or Dec
    if bydecl == True:
        funcof = comppos.dec.value
        xbins = np.linspace(-90, 90, int(180/xbinsize_deg))
        x0 = -90
        x1 = 90
        xlab = 'Component Decl. [deg]'
    else:
        funcof = comppos.ra.value
        xbins = np.linspace(0, 360, int(360/xbinsize_deg))
        x0 = 360
        x1 = 0
        xlab = 'Component R.A. [deg]'
            
    ##incorporate sepunit into string for ylabel
    ylab = 'Angular separation from component-set mean \n [' + sepunit +']'
                
    ##plot figure
    fig = plt.figure(figsize=(6.5,6))
    plt.hist2d(funcof, sep, [xbins, sepbins], cmap=cmap, norm=LogNorm())
    plt.yscale('log')
    plt.xlim(x0, x1)
    plt.ylabel(ylab, fontsize=fontsize)
    plt.xlabel(xlab, fontsize=fontsize)
    plt.title(title, fontsize=fontsize+2)
    plt.colorbar(label=r'$N$')
    plt.tight_layout()

    if savefig==True:
        plt.savefig(outname)
        plt.close()
    
    return


def plot_posdensity(ra, dec,
                    dlim=(-40, 90),
                    alim=(0, 360),
                    binsize_deg=1,
                    cmap='viridis',
                    fontsize=13,
                    title='on-sky density of TM data',
                    savefig=False,
                    outname='skydensity.png'):
    
    'plot density of components/sources by position'
    ##only useful for catching first order issues like "are we missing half the sky!"
    
    ##ensure ra/dec are arrays - coords with units wont bin
    ra = np.array(ra)
    dec = np.array(dec)
    
    ##set up bins based on coordrange
    abins = np.linspace(alim[0], alim[1], int((alim[1]-alim[0])/binsize_deg))
    dbins = np.linspace(dlim[0], dlim[1], int((dlim[1]-dlim[0])/binsize_deg))
    
    ###define ticks
    aticks = np.arange(360, -1, -60).astype(int)
    dticks = np.arange(-90, 91, 30).astype(int)
    
    ##plot figure
    fig = plt.figure(figsize=(6.5,6))
    plt.hist2d(ra, dec, [abins, dbins], norm=LogNorm(), cmap=cmap)
    plt.xlim(360, 0) ##reverse x for RA
    plt.ylim(-90, 90)
    plt.xlabel('R.A. [deg]', fontsize=fontsize)
    plt.ylabel('Decl. [deg]', fontsize=fontsize)
    plt.title(title, fontsize=fontsize+2)
    plt.colorbar(label=r'$N$')
    plt.xticks(aticks)
    plt.yticks(dticks)
    plt.tight_layout()
    
    if savefig==True:
        plt.savefig(outname)
        plt.close()
    
    return



def parse_args():
    "parse input args, i.e. input file(s)"
    parser = argparse.ArgumentParser(description="produce diagnostic plots based for CIRADA transients marshal")
    parser.add_argument("--prejoined", action='store', type=str, default='True',
                        help="using data that has been queried such that the individual tables are already joined")
    parser.add_argument("--data", action='store', type=str, help="prejoined data file")
    parser.add_argument("--components", action='store', type=str,
                        help="tm_temporalcomponent")
    parser.add_argument("--setcomponents", action='store', type=str,
                        help="tm_setcomponent")
    parser.add_argument("--sets", action='store', type=str,
                        help="tm_temporalcomponentset")
    parser.add_argument("--candidates", action='store', type=str,
                        help="tm_candidate")
    
    args = parser.parse_args()
    
    ##convert args.prejoined to bool
    args.prejoined = bool(util.strtobool(args.prejoined))
                        
    return args


def make_diagnostic_plots(args,
                          output_dir='tm_diagnostic_plots'):
    'load appropriate data set and produce plots'
    
    if args.prejoined == True:
        if args.data is None:
            raise ValueError('Transient Marshal data not provided')
        else:
            data = load_prejoined(args.data)
    
    else:
        if args.components is None:
            raise ValueError('Component data not provided. If prejoined is False the Transient Marshal tables temporalcomponent, temporalcomponentset, setcomponents and candidate need to be provided')
        if args.sets is None:
            raise ValueError('Component-set data not provided. If prejoined is False the Transient Marshal tables temporalcomponent, temporalcomponentset, setcomponents and candidate need to be provided')
        if args.setcomponents is None:
            raise ValueError('Set-component data not provided. If prejoined is False the Transient Marshal tables temporalcomponent, temporalcomponentset, setcomponents and candidate need to be provided')
        if args.candidates is None:
            raise ValueError('Candidate data not provided. If prejoined is False the Transient Marshal tables temporalcomponent, temporalcomponentset, setcomponents and candidate need to be provided')
        
        else:
            data = load_separate_and_join(candfile=args.candidates,
                                      scfile=args.setcomponents,
                                      cfile=args.components,
                                      csfile=args.sets)

    ###make directory to put plots in if not already present
    
    
    dirlist = os.listdir()
    if output_dir is not None:
        if output_dir!='':
            dirlist = os.listdir()
            if output_dir not in dirlist:
                os.mkdir(output_dir)
            if output_dir[len(output_dir)-1]!='/':
                output_dir =  output_dir + '/'
    else:
        output_dir=''

    ### MAKE PLOTS
    ##sky density plot
    plot_posdensity(ra=data['RA'], dec=data['DEC'],
                    savefig=True, outname=output_dir+'tm_skydensity.png')

    ##sky separation of components from set mean
    plot_compsep_dist(ra_comp=data['RA'], dec_comp=data['DEC'],
                      ra_set=data['mean_RA'], dec_set=data['mean_DEC'],
                      minbin=0.03, savefig=True,
                      outname=output_dir+'comp-set_separations.png')

    plot_sep_by_coord(ra_comp=data['RA'], dec_comp=data['DEC'],
                      ra_set=data['mean_RA'], dec_set=data['mean_DEC'],
                      minbin=0.03, savefig=True,
                      outname=output_dir+'comp-set_separations_by_dec.png')

    plot_sep_by_coord(ra_comp=data['RA'], dec_comp=data['DEC'],
                      ra_set=data['mean_RA'], dec_set=data['mean_DEC'],
                      bydecl=False, minbin=0.03,
                      savefig=True, outname=output_dir+'comp-set_separations_by_ra.png')

    ##fraction of dissapearing sources and compare flux distributions
    ###create signal to noise column for aperture flux and plot this too
    fluxcol='FluxMax'
    sncolname = 'FluxMax/E_FluxMax'
    data[sncolname] = data['FluxMax']/data['E_FluxMax']

    disappeared = data[(data['disappeared']=='t')]


    plot_fraction_of_disap(alldat=data[fluxcol], disdat=disappeared[fluxcol],
                           lower_data_limit=0.3, upper_data_limit=100,
                           xlab=fluxcol + ' [mJy]',
                           title='Fraction of dissapearing sources as a function of \n brightest aperture flux measurement',
                           savefig=True, outname=output_dir+'disap_fraction_by_flux.png')

    plot_fraction_of_disap(alldat=data[sncolname], disdat=disappeared[sncolname],
                           lower_data_limit=1, upper_data_limit=1000,
                           xlab=sncolname,
                           title='Fraction of dissapearing sources as a function of \n brightest aperture S/N',
                           savefig=True, outname=output_dir+'disap_fraction_by_signoise.png')

    ###create flux ratio column and plot this versus sig/noise
    fratcolname = 'FluxMax/FluxMin'
    data[fratcolname] = data['FluxMax']/data['FluxMin']

    plot_fluxratio_vs_sn(fluxratio=data[fratcolname], signoise=data[sncolname],
                         xlab=sncolname, ylab=fratcolname,
                         savefig=True, outname=output_dir+'fluxratio_vs_signoise.png')

    compare_flux_dists(s_main=data[fluxcol], s_comp=disappeared[fluxcol],
                       xlab=fluxcol+' [mJy]',
                       lab0='All components', lab1='Disappearing components',
                       savefig=True, outname=output_dir+'all_v_dissap_flux_comparison.png')


    return



#######################################################################
#######################################################################
###main -- write readme


if __name__ == '__main__':
    args = parse_args()
    make_diagnostic_plots(args)











