**tm_diagnostics readme**

command line code for producing basic diagnostic plots for the CIRADA transients marshal. Can be run by pointing to a file resulting from a query that has joined individual tables in TM database, or by pointing to files of the individual tables.


**Running**

If pointing to a single file output by a query that joins at least the tables *temporalcomponent*, *temporalcomponentset* and *candidate* run by calling:

*python tm_diagnostic_plots.py --data filename*

i.e. using the test data provided in this repo one would call:

*python tm_diagnostic_plots.py --data test_data/test_prejoined_tmdata.fits*


Alternatively individual files for *temporalcomponent*, *setcomponents*, *temporalcomponentset* and *candidate* can be pointed to and these will be joined in the code by calling:

*python tm_diagnostic_plots.py --prejoined False --components filename --setcomponents filename --sets filename --candidates filename*

again, using the testdata provided in this repo as an example:

*python tm_diagnostic_plots.py --prejoined False --components test_data/test_tmcomponent.fits --setcomponents test_data/test_tmsetcomponents.fits --sets test_data/test_tmcomponentset.fits --candidates test_data/test_tmcandidates.fits*


**Output**

Outputs 8 plots to the folder *tm_diagnostic_plots/*, explicitly these are:

 - *tm_skydensity.png* is a density plot of RA vs Dec for transient marshall components,
 - *comp-set_separations.png* is a histogram of the angular separation between a component's position and the mean position of the component set to which it assigned,
 - *comp-set_separations_by_dec.png* is a 2D histogram showing *comp-set_separations.png* as a function of component declination,
 - *comp-set_separations_by_ra.png* is a 2D histogram showing *comp-set_separations.png* as a function of component right ascension,
 - *fluxratio_vs_signoise.png* shows the ratio between of maximum to minimum aperture flux for a component set as a function of the signal to noise on the brightest aperture flux measurement,
  - *disap_fraction_by_flux.png* shows the fraction of sets classed as disappearing as a function of their brightest aperture flux measurement,
 - *disap_fraction_by_signoise.png* shows the fraction of sets classed as disappearing as a function of the signal-to-noise on their brightest aperture flux measurement,
 - *all_v_dissap_flux_comparison.png* compares the distribution of the brightest aperture flux measurements for disappearing component sets with the distribution for all component sets.

 
 
 
